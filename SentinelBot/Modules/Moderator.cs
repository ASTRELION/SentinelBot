﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Addons.InteractiveCommands;

namespace SentinelBot
{
    [RequireContext(ContextType.Guild)]
    [EnabledCommand]
    [CustomPerms]
    [Remarks("2")]
    public class Moderator : InteractiveModuleBase
    {
        private CommandService cmdServ;
        private IDependencyMap map;

        // Get command service for accessing commands
        public Moderator(CommandService cmdServ, IDependencyMap map)
        {
            this.cmdServ = cmdServ;
            this.map = map;
        }

        // /bug
        [Command("bug")]
        [Summary("Sent a bug report to the bot owner. Include subject and message seperated by |")]
        [RequireUserPermission(GuildPermission.ManageGuild)]
        public async Task BugReport(
            [Summary("Bug report, seperate subject and message with |"), Remainder] string report)
        {
            string[] bugreport = report.Trim().Split('|');

            if (bugreport.Count() == 2)
            {
                IUser owner = (await Context.Client.GetApplicationInfoAsync()).Owner;
                IDMChannel dm = await owner.CreateDMChannelAsync();

                EmbedBuilder e = new EmbedBuilder
                {
                    Title = "Bug Report",
                    Description = "\u200B",
                    Color = Colors.blue,
                    Footer = new EmbedFooterBuilder
                    {
                        Text = Context.User.ToString() + " | " + DateTime.Now.ToLongDateString() + " at " + DateTime.Now.ToLongTimeString()
                    }
                };

                EmbedFieldBuilder f = new EmbedFieldBuilder
                {
                    Name = bugreport[0],
                    Value = bugreport[1]
                };

                e.AddField(f);
                await dm.SendMessageAsync("", embed: e);
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await ReplyAsync("Invalid syntax. Do /bug <subject message> | <message>");
            }
        }

        // /resetother <user>
        [Command("resetother")]
        [Summary("Reset another user's nickname")]
        public async Task Reset(
            [Summary("User to reset nickname for")] IGuildUser user)
        {
            try
            {
                await user.ModifyAsync(x => x.Nickname = user.Username);
                await CommandConfirmation.Success(Context);
            }
            catch
            {
                await CommandConfirmation.Failure(Context);
            }  
        }

        // /reset everyone
        [Command("resetother")]
        [Summary("Reset all nicknames")]
        public async Task Reset(
            [Summary("Everyone role")] string everyone)
        {
            if (everyone.Equals(Context.Guild.EveryoneRole.Name) || everyone.ToUpper().Equals("EVERYONE"))
            {
                foreach (IGuildUser user in await Context.Guild.GetUsersAsync())
                {
                    try
                    {
                        await user.ModifyAsync(x => x.Nickname = user.Username);
                    }
                    catch { }
                }

                await CommandConfirmation.Success(Context);
            }
        }

        // /deafen
        [Command("deafen")]
        [Summary("Server deafens a user")]
        public async Task Deafen(
            [Summary("User to deafen")] IGuildUser user,
            [Summary("Time to deafen in minutes, 0 for permanent")] int deafenTime = 0,
            [Summary("Deafen reason"), Remainder] string reason = "")
        {
            if (user.IsDeafened)
            {
                await user.ModifyAsync(x => x.Deaf = false);
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await user.ModifyAsync(x => x.Deaf = true);
                await CommandConfirmation.Success(Context);

                PerServerConfig config = PerServerConfig.Load(Context.Guild);
                await ModLog.LogMod(await Context.Guild.GetUserAsync(Context.User.Id), user, "Deafen", reason, deafenTime, config);
            }   
        }

        // /mute
        [Command("mute")]
        [Summary("Server mutes a user")]
        public async Task Mute(
            [Summary("User to mute")] IGuildUser user, 
            [Summary("Time to mute in minutes, 0 for permanent")] int muteTime = 0,
            [Summary("Mute reason"), Remainder] string reason = "")
        {
            if (user.IsMuted)
            {
                await user.ModifyAsync(x => x.Mute = false);
                await ReplyAsync($"`{user.Username} unmuted.`");
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await user.ModifyAsync(x => x.Mute = true);
                await ReplyAsync($"`{user.Username} muted.`");

                PerServerConfig config = PerServerConfig.Load(Context.Guild);
                await ModLog.LogMod(await Context.Guild.GetUserAsync(Context.User.Id), user, "Mute", reason, muteTime, config);
                await CommandConfirmation.Success(Context);
            }  
        }

        // /textmute
        [Command("textmute")]
        [Summary("Text mutes a user (they cannot send messages)")]
        public async Task TextMute([Summary("User to mute")] IGuildUser user,
            [Summary("Time to mute in minutes, 0 for permanent")] int muteTime = 0,
            [Summary("Mute reason"), Remainder] string reason = "")
        {
            bool muted = false;
            var channels = await Context.Guild.GetTextChannelsAsync();

            foreach (ITextChannel c in channels)
            {
                if (c.GetPermissionOverwrite(user) != null) // Removes mute if already on
                {
                    await c.RemovePermissionOverwriteAsync(user);
                    await CommandConfirmation.Success(Context);
                }
                else // Adds mute if not on
                {
                    OverwritePermissions p = OverwritePermissions.InheritAll;
                    p = p.Modify(sendMessages: PermValue.Deny);
                    await c.AddPermissionOverwriteAsync(user, p);
                    await CommandConfirmation.Success(Context);
                    muted = true;
                }
            }

            if (muted)
            {
                PerServerConfig config = PerServerConfig.Load(Context.Guild);
                await ModLog.LogMod(await Context.Guild.GetUserAsync(Context.User.Id), user, "TextMute", reason, muteTime, config); 
            }   
        }

        // /kick
        [Command("kick")]
        [Summary("Kicks a user")]
        public async Task Kick(
            [Summary("User to kick")] IGuildUser user,
            [Summary("Kick reason"), Remainder] string reason = "")
        {
            await user.KickAsync();
            await ReplyAsync($"`{Context.User.Username} kicked {user.Username}`");

            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            await ModLog.LogMod(await Context.Guild.GetUserAsync(Context.User.Id), user, "Kick", reason, 0, config);
            await CommandConfirmation.Success(Context);
        }

        // /ban
        [Command("ban")]
        [Summary("Bans a user")]
        public async Task Ban(
            [Summary("User to ban")] IGuildUser user,
            [Summary("Time to ban in minutes, 0 for permanent")] int banTime = 0,
            [Summary("Ban reason"), Remainder] string reason = "")
        {
            await Context.Guild.AddBanAsync(user);
            await ReplyAsync($"`{Context.User.Username} banned {user.Username}`");

            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            await ModLog.LogMod(await Context.Guild.GetUserAsync(Context.User.Id), user, "Ban", reason, banTime, config);
            await CommandConfirmation.Success(Context);
        }

        // /unban
        [Command("unban")]
        [Summary("Unbans a user")]
        public async Task UnBan(
            [Summary("User to unban")] IGuildUser user)
        {
            await Context.Guild.RemoveBanAsync(user);
            await ReplyAsync($"`{Context.User.Username} unbanned {user.Username}`");
            await CommandConfirmation.Success(Context);
        }

        // /say
        [Command("say")]
        [Summary("Says a message to the current channel")]
        public async Task Say(
            [Summary("Text to say"), Remainder] string message)
        {
            await Context.Message.DeleteAsync();
            await ReplyAsync($"```{message}```");
        }

        // /purge
        [Command("purge", RunMode = RunMode.Async)]
        [Summary("Deletes given amount of messages from current channel")]
        public async Task Purge(
            [Summary("Amount of messages to delete (max:100)")] int count = 100, 
            [Summary("User to delete messages from")] IGuildUser user = null)
        {
            await Context.Message.DeleteAsync();
            
            if (user == null)
            {
                var msgs = await Context.Channel.GetMessagesAsync(count).Flatten();
                await Context.Channel.DeleteMessagesAsync(msgs);
                await ReplyAsync("```\n" +
                    $"{Context.User.Username} deleted {msgs.Count()} messages from the channel.\n" +
                    "```", deleteAfter: 10);
            }
            else
            {
                try
                {
                    var msgs = await Context.Channel.GetMessagesAsync(count).Flatten();
                    await Context.Channel.DeleteMessagesAsync(msgs.Where(x => x.Author.Equals(user)));
                    await ReplyAsync("```\n" +
                        $"{Context.User.Username} deleted {msgs.Count()} messages from the user {user.Username} from the channel.\n" +
                        "```", deleteAfter: 10);
                }
                catch { }            
            }
        }

        // /moveall
        [Command("moveall")]
        [Summary("Move everyone in your current channel to a new channel")]
        public async Task MoveAll(
            [Summary("Channel to move to"), Remainder] string channelName)
        {
            // preforms /moveto but with multiple users
            IGuildUser user = Context.User as IGuildUser;
            IVoiceChannel curr = user.VoiceChannel;
            var users = curr.GetUsersAsync().Flatten().Result; // List of users in the channel
            IVoiceChannel dest = null;
            SocketVoiceChannel destS = (SocketVoiceChannel)dest;

            try
            {
                dest = (await Context.Guild.GetVoiceChannelsAsync()) // Find the first occurance of channel requested
                    .Where(x => x.Name.ToUpper().Equals(channelName.ToUpper())).First();
            }
            catch
            {
                await CommandConfirmation.Failure(Context);
            }

            // Move current users to new channel
            if (dest != null)
            {
                foreach (var u in users)
                    if (u.GetPermissions(dest).Connect)
                        await u.ModifyAsync(x => x.ChannelId = dest.Id);

                await CommandConfirmation.Success(Context);
            }
        }  
    }
}
