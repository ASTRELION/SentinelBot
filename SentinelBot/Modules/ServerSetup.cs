﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.Addons.InteractiveCommands;

namespace SentinelBot
{
    // /serversetup
    [Group("ServerSetup"), RequireContext(ContextType.Guild)]
    [Alias("setup")]
    [RequireUserPermission(GuildPermission.Administrator)]
    [Remarks("3")]
    public class ServerSetup : InteractiveModuleBase // Have all commands be confirmed
    {
        private CommandService cmdServ;
        private IDependencyMap map;

        // Get command service for accessing commands
        public ServerSetup(CommandService cmdServ, IDependencyMap map)
        {
            this.cmdServ = cmdServ;
            this.map = map;
        }

        [Command("create")]
        [Summary("DELETES current roles, channels, and settings and replaces with defaults")]
        public async Task Create()
        {
            await ReplyAsync("Please wait while I recreate your server for you...");

            IGuild guild = Context.Guild;
            bool hasBotRole = false;

            // DELETE CURRENT
            foreach (IRole role in guild.Roles)
            {
                if (role != guild.EveryoneRole && role.Name.ToUpper() != "BOT")
                    await role.DeleteAsync();
                if (role.Name.ToUpper().Equals("BOT"))
                    hasBotRole = true;
            }
            
            foreach (ITextChannel text in await guild.GetTextChannelsAsync())
                if (text.Id != guild.DefaultChannelId)
                    await text.DeleteAsync();

            foreach (IVoiceChannel voice in await guild.GetVoiceChannelsAsync())
                await voice.DeleteAsync();
            
            // CREATE NEW
            // Roles
            // Bot/Owner
            IRole botRole = null;
            if (!hasBotRole)
            {
                botRole = await guild.CreateRoleAsync("Bot", GuildPermissions.All, Colors.blue);
                await botRole.ModifyAsync(x => x.Hoist = true);
            }
                
            IRole ownerRole = await guild.CreateRoleAsync("Owner", GuildPermissions.All, Colors.blue);
            await ownerRole.ModifyAsync(x => x.Hoist = true);
            // Admin
            GuildPermissions perms = GuildPermissions.All;
            perms = perms.Modify(administrator: false, manageRoles: false, banMembers: false, manageWebhooks: false, sendTTSMessages: false, manageGuild: false);

            IRole adminRole = await guild.CreateRoleAsync("Admin", perms, Colors.red);
            await adminRole.ModifyAsync(x => x.Hoist = true);

            // Mod
            perms = perms.Modify(manageChannels: false, kickMembers: false, manageEmojis: false, deafenMembers: false);

            IRole modRole = await guild.CreateRoleAsync("Mod", perms, Colors.purple);
            await modRole.ModifyAsync(x => x.Hoist = true);
            
            // Text Channels
            ITextChannel infoTC = await guild.GetDefaultChannelAsync();
            await infoTC.ModifyAsync(x => x.Name = "default");
            ITextChannel mainTC = await guild.CreateTextChannelAsync("main_chat");
            ITextChannel logTC = await guild.CreateTextChannelAsync("log");

            await infoTC.AddPermissionOverwriteAsync(guild.EveryoneRole, new OverwritePermissions(sendMessages: PermValue.Deny));
            await logTC.AddPermissionOverwriteAsync(guild.EveryoneRole, new OverwritePermissions(sendMessages: PermValue.Deny));
            
            // Voice Channels
            IVoiceChannel staffVC = await guild.CreateVoiceChannelAsync("Staff Channel");
            IVoiceChannel public1VC = await guild.CreateVoiceChannelAsync("Public 1");
            IVoiceChannel public2VC = await guild.CreateVoiceChannelAsync("Public 2");
            IVoiceChannel public3VC = await guild.CreateVoiceChannelAsync("Public 3");
            IVoiceChannel privVC = await guild.CreateVoiceChannelAsync("-Private Channels-");
            IVoiceChannel afkVC = await guild.CreateVoiceChannelAsync("AFK");

            await guild.ModifyAsync(x => x.AfkChannelId = afkVC.Id);
            await staffVC.AddPermissionOverwriteAsync(guild.EveryoneRole, new OverwritePermissions(connect: PermValue.Deny));
            await staffVC.AddPermissionOverwriteAsync(ownerRole, new OverwritePermissions(connect: PermValue.Allow));
            await staffVC.AddPermissionOverwriteAsync(adminRole, new OverwritePermissions(connect: PermValue.Allow));
            await staffVC.AddPermissionOverwriteAsync(modRole, new OverwritePermissions(connect: PermValue.Allow));
            await privVC.AddPermissionOverwriteAsync(guild.EveryoneRole, new OverwritePermissions(connect: PermValue.Deny));

            // Settings
            await mainTC.SendMessageAsync("/settings mainchannel");
            await logTC.SendMessageAsync("/settings logchannel");
            await mainTC.SendMessageAsync("/settings logmode MOD");
            await mainTC.SendMessageAsync("/settings pcn 6");
            await mainTC.SendMessageAsync("/settings greeting Welcome [user] to the server!");
            await mainTC.SendMessageAsync("/settings farewell [user] has left the server!");
            await mainTC.SendMessageAsync("/settings enable all");
            await mainTC.SendMessageAsync("/settings defperms");

            await CommandConfirmation.Success(Context);
            await mainTC.SendMessageAsync("Done! Do /settings to check your new current server settings. Remember to add roles to users!");
        }

        [Command("destroy")]
        [Summary("DELETES current roles, channels, and settings")]
        public async Task Destroy()
        {
            await ReplyAsync("Please wait while I remove your current items...");
            IGuild guild = Context.Guild;

            // DELETE CURRENT
            foreach (IRole role in guild.Roles)
                if (role != guild.EveryoneRole && role.Name.ToUpper() != "BOT")
                    await role.DeleteAsync();

            foreach (ITextChannel text in await guild.GetTextChannelsAsync())
                if (text.Id != guild.DefaultChannelId)
                    await text.DeleteAsync();

            foreach (IVoiceChannel voice in await guild.GetVoiceChannelsAsync())
                await voice.DeleteAsync();

            await (await guild.GetDefaultChannelAsync()).SendMessageAsync("Done!");
        }
    }
}
