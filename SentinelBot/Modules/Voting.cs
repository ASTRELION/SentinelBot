﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Addons.InteractiveCommands;

namespace SentinelBot
{
    [RequireContext(ContextType.Guild)]
    [EnabledCommand]
    [CustomPerms]
    [Remarks("2")]
    public class Voting : InteractiveModuleBase
    {
        private CommandService cmdServ;
        private IDependencyMap map;

        // Get command service for accessing commands
        public Voting(CommandService cmdServ, IDependencyMap map)
        {
            this.cmdServ = cmdServ;
            this.map = map;
        }

        [Command("votestart")]
        [Summary("Start a vote for listed items, seperate by commas or use `@everyone`")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task VoteStart(
            [Summary("Items to vote for"), Remainder] string things)
        {
            await Context.Message.DeleteAsync();
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            // Check if a vote is already in progress
            if (config.Votes.Count == 0)
            {
                EmbedBuilder e = new EmbedBuilder
                {
                    Title = "__Voting Started__",
                    Description = "Vote for one of the following items by using `/vote <item>`",
                    Color = Colors.blue
                };

                EmbedFieldBuilder f = new EmbedFieldBuilder
                {
                    Name = "__Items:__",
                    Value = "\u200B"
                };
                
                if (things.Equals("@everyone"))
                {
                    IGuildUser[] users = (await Context.Guild.GetUsersAsync()).ToArray();
                    e.Description = "Vote for a user by copying their username from the list and typing `/vote <full username>`";

                    for (int i = 0; i < users.Length; i++)
                    {
                        f.Value += users[i] + "\n";
                        config.Votes.Add(users[i].ToString(), 0);
                        config.Save();
                    }
                }
                else
                {
                    string[] items = things.Trim().Split(',');

                    // Add items to voting list
                    for (int i = 0; i < items.Length; i++)
                    {
                        f.Value += items[i].Trim() + "\n";
                        config.Votes.Add(items[i], 0);
                        config.Save();
                    }
                }

                e.AddField(f);
                await ReplyAsync("", embed: e);
            }
            else
            {
                await ReplyAsync("A vote is already in progress; stop it with /votestop and try again.");
            }    
        }

        [Command("votestop")]
        [Summary("Stop and display results for a current vote in-progress")]
        [RequireUserPermission(GuildPermission.Administrator)]
        public async Task VoteStop()
        {
            await Context.Message.DeleteAsync();
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (config.Votes.Count > 0)
            {
                EmbedBuilder e = new EmbedBuilder
                {
                    Title = "__Vote Results__",
                    Color = Colors.blue
                };

                EmbedFieldBuilder f1 = new EmbedFieldBuilder
                {
                    Name = "__Item__",
                    Value = "\u200B",
                    IsInline = true
                };

                EmbedFieldBuilder f2 = new EmbedFieldBuilder
                {
                    Name = "__Votes__",
                    Value = "\u200B",
                    IsInline = true
                };

                var results = config.Votes;

                foreach (var r in results)
                {
                    f1.Value += r.Key + "\n";
                    f2.Value += $"\t {r.Value}\n";
                }

                e.AddField(f1);
                e.AddField(f2);

                await ReplyAsync("", embed: e);
                config.Votes.Clear();
                config.Voters.Clear();
                config.Save();
            }
            else
            {
                await ReplyAsync("There aren't any votes in progress, use /votestart to start one.");
            }
        }

        [Command("vote")]
        [Summary("Vote for an item")]
        public async Task Vote(
            [Summary("Item to vote for")] string item)
        {
            await Context.Message.DeleteAsync();
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (!config.Voters.Contains(Context.User.Id) && config.Votes.ContainsKey(item))
            {
                config.Voters.Add(Context.User.Id);
                config.Votes[item] = config.Votes[item] + 1;
                config.Save();
                
                int votersLeft = (await Context.Guild.GetUsersAsync()).Count - config.Voters.Count;

                if (votersLeft <= 0)
                {
                    await VoteStop();
                }
                else
                {
                    await ReplyAsync($"Users left to vote: {votersLeft}");
                }
            }
            else
            {
                await ReplyAsync("Voting error. Check your spelling.");
            }
        }
    }
}
