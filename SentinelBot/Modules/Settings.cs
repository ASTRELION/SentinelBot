﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;

using Discord;
using Discord.WebSocket;
using Discord.Commands;
using Discord.Addons.InteractiveCommands;
using System.Collections;

namespace SentinelBot
{
    // Editable server settings
    [Group("Settings"), RequireContext(ContextType.Guild)]
    [Alias("ss", "setting")]
    [Remarks("3")]
    [RequireUserPermission(GuildPermission.Administrator)]
    public class Settings : InteractiveModuleBase
    {
        private CommandService cmdServ;
        private IDependencyMap map;

        // Get command service for accessing commands
        public Settings(CommandService cmdServ, IDependencyMap map)
        {
            this.cmdServ = cmdServ;
            this.map = map;
        }

        [Command()]
        [Summary("Display all server settings and their values")]
        public async Task AllSettings()
        {
            EmbedBuilder e = new EmbedBuilder
            {
                Title = $"__{Context.Guild.Name} Settings__",
                Description = "\u200B",
                Color = Colors.blue
            };

            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            PropertyInfo[] props = config.GetType().GetProperties();

            foreach (var p in props)
            {
                e.Description += $"**{p.Name}:** ";

                if (p.GetValue(config, null) is IList)
                {
                    IList list = p.GetValue(config, null) as IList;

                    if (list.Count > 0 && list[0] is IList)
                    {
                        for (int x = 0; x < list.Count; x++)
                        {
                            IList list2 = list[x] as IList;

                            for (int y = 0; y < list2.Count; y++)
                            {
                                if (y == list2.Count - 1)
                                    e.Description += $"{list2[y].ToString()})]";
                                else if (y == 0)
                                    e.Description += $"[{list2[y]} (";
                                else
                                    e.Description += $"{list2[y]}, ";
                            }

                            if (x != list.Count - 1)
                                e.Description += ", ";
                        }
                    }
                    else
                    {
                        for (int x = 0; x < list.Count; x++)
                        {
                            if (x != list.Count - 1)
                                e.Description += $"{list[x].ToString()}, ";
                            else
                                e.Description += $"{list[x].ToString()}";
                        }
                    }
                }
                else if (p.GetValue(config, null) is IDictionary)
                {
                    IDictionary dict = p.GetValue(config, null) as IDictionary;
                    
                    foreach (var k in dict.Keys)
                    {
                        e.Description += $"[{k}:{dict[k]}] ";
                    }
                }
                else
                {
                    e.Description += $"{p.GetValue(config)}";
                }

                e.Description += "\n";
            }

            await ReplyAsync(Context.User.Mention, embed: e);
        }

        [Command("enable")]
        [Summary("Enable a command or ALL")]
        public async Task Enable(
            [Summary("Command category to enable (eg. channel, ping, etc)")] string command)
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (command.ToUpper().Equals("ALL"))
            {
                config.EnabledCommands.Clear();
                config.EnabledCommands.AddRange(Commands.GetCommands(cmdServ));
                await CommandConfirmation.Success(Context);
            }
            else if (!config.EnabledCommands.Contains(command.ToUpper()) && Commands.GetCommands(cmdServ).Contains(command.ToUpper()))
            {
                config.EnabledCommands.Add(command.ToUpper());
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }

            config.Save();
        }
        
        [Command("disable")]
        [Summary("Disable a command")]
        public async Task Disable(
            [Summary("Command category to disable (eg. channel, ping, etc)")] string command)
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (command.ToUpper().Equals("ALL"))
            {
                config.EnabledCommands.Clear();
                config.Save();
                await CommandConfirmation.Success(Context);
            }
            else if (config.EnabledCommands.Contains(command.ToUpper()))
            {
                config.EnabledCommands.Remove(command.ToUpper());
                config.Save();
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        [Command("commands")]
        [Summary("List all commands able to enable/disable and alter permissions to")]
        [Alias("cmds")]
        public async Task Command()
        {
            EmbedBuilder e = new EmbedBuilder
            {
                Title = "__Editable Commands__",
                Description = "All of the following commands/categories can be enabled/disabled and have their permissions altered",
                Color = Colors.blue
            };

            List<string> cmds = Commands.GetCommands(cmdServ);
            EmbedFieldBuilder f1 = new EmbedFieldBuilder { Name = "\u200B", Value = "\u200B", IsInline = true };
            EmbedFieldBuilder f2 = new EmbedFieldBuilder { Name = "\u200B", Value = "\u200B", IsInline = true };

            for (int x = 0; x < cmds.Count / 2; x++)
                f1.Value += cmds[x] + "\n";

            for (int y = cmds.Count / 2; y < cmds.Count; y++)
                f2.Value += cmds[y] + "\n";

            e.AddField(f1);
            e.AddField(f2);

            await ReplyAsync("", embed: e);
        }

        [Command("permissions")]
        [Summary("List all permissions you are able to add to commands")]
        public async Task Permissions()
        {
            EmbedBuilder e = new EmbedBuilder
            {
                Title = "__Permissions__",
                Description = "The following are the permissions you can add to commands",
                Color = Colors.blue
            };

            List<GuildPermission> perms = GuildPermissions.All.ToList();
            EmbedFieldBuilder f1 = new EmbedFieldBuilder { Name = "\u200B", Value = "\u200B", IsInline = true };
            EmbedFieldBuilder f2 = new EmbedFieldBuilder { Name = "\u200B", Value = "\u200B", IsInline = true };

            for (int x = 0; x < perms.Count / 2; x++)
                f1.Value += perms[x].ToString() + "\n";

            for (int y = perms.Count / 2; y < perms.Count; y++)
                f2.Value += perms[y].ToString() + "\n";

            e.AddField(f1);
            e.AddField(f2);
            await ReplyAsync("", embed: e);
        }

        [Command("addpermission")]
        [Summary("Give a command a permission")]
        [Alias("addperm")]
        public async Task AddPermission(
            [Summary("Permission to add")] GuildPermission permission, 
            [Summary("Command to add permssion to")] string command)
        {
            if (Commands.GetCommands(cmdServ).Contains(command.ToUpper()))
            {
                PerServerConfig config = PerServerConfig.Load(Context.Guild);

                if (config.CommandPermissions.ContainsKey(command.ToUpper()))
                    config.CommandPermissions[command.ToUpper()] = permission.ToString();
                else
                    config.CommandPermissions.Add(command, permission.ToString());

                config.Save();
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        [Command("delpermission")]
        [Summary("Remove permissions from a command")]
        [Alias("delperm")]
        public async Task DelPermission(
            [Summary("Command to remove permissions from")] string command)
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (Commands.GetCommands(cmdServ).Contains(command.ToUpper()) && config.CommandPermissions.ContainsKey(command.ToUpper()))
            {      
                config.CommandPermissions.Remove(command.ToUpper());
                config.Save();

                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        [Command("defaultpermissions")]
        [Summary("Load default permissions")]
        [Alias("defperms")]
        public async Task DefaultPermissions()
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            config.CommandPermissions.Clear();

            config.CommandPermissions.Add("RESET", "ChangeNickname");
            config.CommandPermissions.Add("MOVETO", "Connect");
            config.CommandPermissions.Add("RESETOTHER", "ManageNicknames");
            config.CommandPermissions.Add("DEAFEN", "DeafenMembers");
            config.CommandPermissions.Add("MUTE", "MuteMembers");
            config.CommandPermissions.Add("TEXTMUTE", "MuteMembers");
            config.CommandPermissions.Add("BAN", "BanMembers");
            config.CommandPermissions.Add("UNBAN", "BanMembers");
            config.CommandPermissions.Add("SAY", "ManageMessages");
            config.CommandPermissions.Add("PURGE", "ManageMessages");
            config.CommandPermissions.Add("MOVEALL", "MoveMembers");

            config.Save();
            await CommandConfirmation.Success(Context);
        }

        [Command("color")]
        [Summary("Set server color to RGB value")]
        [Alias("servercolor")]
        public async Task ServerColor(
            [Summary("R")] int r, 
            [Summary("G")] int g, 
            [Summary("B")] int b)
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (r >= 0 && r <= 255 && g >= 0 && g <= 255 && b >= 0 && b <= 255)
            {
                config.ServerColor = new int[] { r, g, b };
                config.Save();
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await ReplyAsync("All values must be between 0 and 255");
            }
        }

        [Command("roleonjoin")]
        [Summary("Set a role as a role given to every user who joins the server, NONE to clear")]
        public async Task RoleJoin(
            [Summary("Role name, NONE to clear"), Remainder] string role)
        {
            if (!role.ToUpper().Equals("NONE"))
            {
                try
                {
                    IRole r = Context.Guild.Roles.Where(x => x.Name.ToUpper().Equals(role.ToUpper())).FirstOrDefault();

                    PerServerConfig config = PerServerConfig.Load(Context.Guild);

                    if (!config.RolesOnJoin.Contains(r.Name.ToUpper()))
                    {
                        config.RolesOnJoin.Add(r.Name.ToUpper());
                        config.Save();
                    }

                    await CommandConfirmation.Success(Context);
                }
                catch
                {
                    await CommandConfirmation.Failure(Context);
                }
            }
            else
            {
                PerServerConfig config = PerServerConfig.Load(Context.Guild);
                config.RolesOnJoin.Clear();
                config.Save();
                await CommandConfirmation.Success(Context);
            }  
        }

        [Command("mainchannel")]
        [Summary("Set the current channel as the main chat")]
        public async Task MainChat()
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            config.MainChatID = Context.Channel.Id;
            config.Save();
            await CommandConfirmation.Success(Context);
        }

        [Command("logmode")]
        [Summary("Set logging mode (off by default)")]
        public async Task LogMode(
            [Summary("Set logging to OFF, MOD (logs mod commands), ALL (logs all commands)")] string mode)
        {
            if (mode.ToUpper().Equals("OFF") || mode.ToUpper().Equals("MOD") || mode.ToUpper().Equals("ALL"))
            {
                PerServerConfig config = PerServerConfig.Load(Context.Guild);
                config.LogMode = mode.ToUpper();
                config.Save();
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        [Command("logchannel")]
        [Summary("Set current channel as the log channel")]
        public async Task LogChannel()
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            config.LogChannelID = Context.Channel.Id;
            config.Save();
            await CommandConfirmation.Success(Context);
        }

        [Command("greeting")]
        [Summary("Set the greeting for when someone joins the server")]
        public async Task Greeting(
            [Summary("Greeting, leave empty to turn off, use [user] to reference user's name"), Remainder] string greeting = "")
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            config.Greeting = greeting;
            config.Save();
            await CommandConfirmation.Success(Context);
        }
       
        [Command("farewell")]
        [Summary("Set the farewell for when someone leaves the server")]
        public async Task Farewell(
            [Summary("Farewell, leave empty to turn off, use [user] to reference user's name"), Remainder] string farewell = "")
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            config.Farewell = farewell;
            config.Save();
            await CommandConfirmation.Success(Context);
        }

        [Command("wordban")]
        [Summary("Delete all messages containing listed words or NONE")]
        public async Task WordBan(
            [Summary("Words to ban or NONE")] params string[] words)
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (words[0].ToUpper().Equals("NONE"))
            {
                config.BanWords.Clear();
                config.Save();
            }
            else
            {
                config.BanWords.AddRange(words);
                config.Save();
            }

            await CommandConfirmation.Success(Context);
        }

        [Command("wordalert")]
        [Summary("Alert owner when the following words are used or NONE")]
        public async Task WordAlert(
            [Summary("Words to ban or NONE")] params string[] words)
        {
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (words[0].ToUpper().Equals("NONE"))
            {
                config.AlertWords.Clear();
                config.Save();
            }
            else
            {
                config.AlertWords.AddRange(words);
                config.Save();
            }

            await CommandConfirmation.Success(Context);
        }

        [Command("pcn")]
        [Summary("Private Channel Number (Position where private channels will appear)")]
        public async Task PCN(
            [Summary("Position where private channels will appear")] int index)
        {
            if (index >= 2)
            {
                PerServerConfig c = PerServerConfig.Load(Context.Guild);
                c.PrivIndex = index;
                c.Save();
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await ReplyAsync("Index must be greater than or equal to 2");
            }
        }
    }
}
