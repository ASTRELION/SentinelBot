﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Discord.Addons.InteractiveCommands;

namespace SentinelBot
{
    [RequireContext(ContextType.Guild)]
    [EnabledCommand]
    [CustomPerms]
    [Remarks("1")]
    public class General : InteractiveModuleBase
    {
        private CommandService cmdServ;
        //private IDependencyMap map;
        private IServiceProvider map;

        // Get command service for accessing commands
        public General(CommandService cmdServ, IServiceProvider map)
        {
            this.cmdServ = cmdServ;
            this.map = map;
        }

        [Command("test")]
        public async Task Test()
        {
            Console.WriteLine(DateTime.Now.ToOADate().ToString());
        }

        // /invite
        [Command("invite")]
        [Summary("Get an invite link to the server")]
        public async Task Invite()
        {
            IInvite i = (await Context.Guild.GetInvitesAsync()).FirstOrDefault();
            await ReplyAsync(i.ToString() ?? "No invite available");
        }
        
        // /reset
        [Command("reset")]
        [Summary("Reset your nickname")]
        public async Task Reset()
        {
            IGuildUser u = (Context.User as IGuildUser) ?? null;
            
            try
            {
                await u.ModifyAsync(x => x.Nickname = u.Username); // Reset nickname to username
                await CommandConfirmation.Success(Context);
            }
            catch
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        // /moveto
        [Command("moveto")]
        [Summary("Move yourself to a new channel")]
        public async Task Default(
            [Summary("Channel to move to"), Remainder] string channelName)
        {
            IGuildUser user = Context.User as IGuildUser;
            IVoiceChannel dest = null;

            dest = (await Context.Guild.GetVoiceChannelsAsync()) // Find the first occurance of channel requested
                    .Where(x => x.Name.ToUpper().Equals(channelName.ToUpper())).FirstOrDefault();

            // Move user to new channel
            if (dest != null && user.GetPermissions(dest).Connect)
                await user.ModifyAsync(x => x.ChannelId = dest.Id);
        }

        // /lfg
        [Command("lfg")]
        [Alias("looking", "lookingforgame", "l4g")]
        [Summary("Notify online users you are looking to play a game")]
        public async Task LFG(
            [Summary("Game you want to play"), Remainder] string game)
        {
            await Context.Message.DeleteAsync();
            IGuildUser user = Context.User as IGuildUser;
            await ReplyAsync($"@here **{user.Username}** is looking to play **{game}**!");
        }

        // /choose
        [Command("choose")]
        [Alias("c")]
        [Summary("Choose a random item or number from list")]
        public async Task Choose(
            [Summary("Items to choose from, seperate with commas"), Remainder] string item)
        {
            string[] items = item.Split(',');
            Random rand = new Random();

            if (items.Length == 1 && int.TryParse(items[0], out int num))
                await ReplyAsync($"**{rand.Next(num) + 1}**!");
            else
                await ReplyAsync($"**{items[rand.Next(items.Length)].Trim()}**!");
        }

        // /help
        [Command("help")]
        [Summary("Displays all command info or help on a specified command")]
        public async Task Help(
            [Summary("Command to get help for"), Remainder] string command = "")
        {
            bool exists = true;
            int page;
            IGuildUser user = Context.User as IGuildUser;
            EmbedBuilder e = new EmbedBuilder
            {
                Color = Colors.blue
            };
            
            if (command.Equals(""))
                page = 1;
            else
                int.TryParse(command, out page);

            if (page != 0 && page >= 1 && page <= 3)
                command = "";

            // Get help for ALL commands
            if (command.Equals(""))
            {
                e.Title = "__Command Help__";
                e.Description = "<required parameter>\n" +
                    "[optional parameter]";
                e.WithFooter(x => x.Text = $"Page {page} of 3 | Use /help <page> for more");

                // Gets parents
                foreach (ModuleInfo m in cmdServ.Modules)
                {
                    if (!m.IsSubmodule && m.Remarks != null && (page == 0 ? m.Remarks.Equals("1") : m.Remarks.Equals(page.ToString())))
                    {
                        EmbedFieldBuilder f = new EmbedFieldBuilder
                        {
                            Name = m.Name,
                            Value = "\u200B"
                        };
                        
                        List<CommandInfo> cmds = new List<CommandInfo>();
                        GetSubs(m);
                        // Dynamically cycles through all submodules and adds commands to outer-most parent
                        // Recursion
                        void GetSubs(ModuleInfo mod)
                        {
                            foreach (CommandInfo com in mod.Commands) // Add raw commands
                            {
                                if (com.CheckPreconditionsAsync(Context).Result.IsSuccess)
                                    cmds.Add(com);      
                            } 
                            
                            foreach (ModuleInfo mods in mod.Submodules)
                                GetSubs(mods);
                        }
                        
                        // Sort commands alphabetically
                        cmds.Sort((x, y) => string.Compare(x.Aliases[0], y.Aliases[0]));

                        foreach (CommandInfo c in cmds)
                            f.Value += $"**/{c.Aliases[0]} {getSyntax(c)}** - {c.Summary}\n";
                        
                        // Split field into 2 if it exceeds 1000 characters
                        if (f.Value.ToString().Length > 1000)
                        {
                            f.Value = "\u200B";
                            EmbedFieldBuilder f2 = new EmbedFieldBuilder
                            {
                                Name = $"{m.Name}, cont.",
                                Value = "\u200B"
                            };

                            for (int x = 0; x < cmds.Count / 2; x++)
                            {
                                f.Value += $"**/{cmds[x].Aliases[0]} {getSyntax(cmds[x])}** - {cmds[x].Summary}\n";
                            }

                            for (int y = cmds.Count / 2; y < cmds.Count; y++)
                            {
                                f2.Value += $"**/{cmds[y].Aliases[0]} {getSyntax(cmds[y])}** - {cmds[y].Summary}\n";
                            }

                            if (!f.Value.Equals("\u200B")) e.AddField(f);
                            if (!f2.Value.Equals("\u200B")) e.AddField(f2);
                        }
                        else if (!f.Value.Equals("\u200B"))
                        {
                            e.AddField(f);
                        }   
                    } 
                }
            }
            else // Get specific help for a command
            {
                CommandInfo c = null;
                
                try
                {
                    c = cmdServ.Commands // Find the command requested
                        .Where(x => x.Aliases[0].ToUpper().Equals(command.ToUpper())).FirstOrDefault();

                    e.Title = $"__{char.ToUpper(c.Aliases[0][0]) + c.Aliases[0].Substring(1)} Help__";
                    e.Description = $"*{c.Summary}*\n";

                    EmbedFieldBuilder f1 = new EmbedFieldBuilder
                    {
                        Name = "Syntax",
                        Value = $"/{c.Aliases[0]} {getSyntax(c)}",
                        IsInline = true
                    };

                    string aliases = "\u200B" + string.Join(", ", c.Aliases);

                    EmbedFieldBuilder f2 = new EmbedFieldBuilder
                    {
                        Name = "Aliases",
                        Value = aliases,
                        IsInline = true
                    };

                    e.AddField(f1);
                    e.AddField(f2);

                    // Add parameter descriptions, if any
                    if (c.Parameters.Count > 0)
                    {
                        EmbedFieldBuilder f3 = new EmbedFieldBuilder
                        {
                            Name = "\u200B",
                            Value = "**Parameters:**"
                        };

                        e.AddField(f3);

                        foreach (var p in c.Parameters)
                        {
                            EmbedFieldBuilder f = new EmbedFieldBuilder
                            {
                                Name = $"{p.Name} - {p.Type.Name}",
                                Value = $"Required? *{!p.IsOptional}*\n"
                            };

                            if (p.Summary != null)
                                f.Value += $"Description: *{p.Summary}*";

                            e.AddField(f);
                        }
                    }
                }
                catch
                {
                    try
                    {
                        ModuleInfo m = cmdServ.Modules
                            .Where(x => !x.IsSubmodule && x.Name.ToUpper().Equals(command.ToUpper())).FirstOrDefault();

                        string sum = m.Summary ?? "";
                        e.Title = $"__{m.Name} Help__";
                        e.Description = $"{sum}\n";

                        List<CommandInfo> cmds = new List<CommandInfo>();

                        // Dynamically cycles through all submodules and adds commands to outer-most parent
                        // Recursion
                        void GetSubs(ModuleInfo mod)
                        {
                            foreach (CommandInfo com in mod.Commands) // Add raw commands
                            {
                                if (com.CheckPreconditionsAsync(Context).Result.IsSuccess)
                                    cmds.Add(com);
                            }

                            foreach (ModuleInfo mods in mod.Submodules)
                                GetSubs(mods);
                        }

                        GetSubs(m);
                        cmds.Sort((x, y) => string.Compare(x.Aliases[0], y.Aliases[0]));

                        foreach (CommandInfo co in cmds)
                            e.Description += $"**/{co.Aliases[0]} {getSyntax(co)}** - {co.Summary}\n";
                    }
                    catch
                    {
                        await ReplyAsync("Command does not exist.");
                        exists = false;
                    }
                }        
            }
            
            if (exists)
                await ReplyAsync(Context.User.Mention, embed: e);
        }

        // /afk
        [Command("afk")]
        [Summary("Set yourself as AFK")]
        public async Task AFK(
            [Summary("User to set afk"), Remainder] IGuildUser user = null)
        {
            await Context.Message.DeleteAsync();
            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            IGuildUser u = Context.User as IGuildUser;
            ITextChannel main = await Context.Guild.GetTextChannelAsync(config.MainChatID);
            IRole afkRole;
            
            // Allows bot to AFK users
            if (user != null && u.IsBot)
                u = user;
            
            // Get AFK role if it exists, otherwise create it
            try
            {
                afkRole = Context.Guild.Roles.Where(x => x.Name.ToUpper().Equals("AFK")).FirstOrDefault();
            }
            catch
            {
                afkRole = await Context.Guild.CreateRoleAsync("AFK");
            }

            // AFK or unAFK user
            if (u.RoleIds.Contains(afkRole.Id))
            {
                await u.RemoveRoleAsync(afkRole);
                EmbedBuilder e = new EmbedBuilder
                {
                    Color = Colors.blue,
                    Title = $"{u.Username} is no longer AFK."
                };
                await main.SendMessageAsync("", embed: e);
            }
            else
            {
                await u.AddRoleAsync(afkRole);
                EmbedBuilder e = new EmbedBuilder
                {
                    Color = Colors.blue,
                    Title = $"{u.Username} is now AFK."
                };
                await main.SendMessageAsync("", embed: e);

                // Move user to AFK voice channel if they are in a voice channel
                if (u.VoiceChannel != null && u.VoiceChannel.Id != Context.Guild.AFKChannelId)
                    await u.ModifyAsync(x => x.ChannelId = (ulong)Context.Guild.AFKChannelId);
            }
        }

        // /ping
        [Command("ping")]
        [Summary("Ping the bot")]
        public async Task Ping()
        {
            //DiscordSocketClient client = map.Get<DiscordSocketClient>();
            //await ReplyAsync($"Pong! *{client.Latency}ms*");
        }

        // /info
        [Command("botinfo")]
        [Summary("Get info about the bot")]
        public async Task BotInfo()
        {
            IApplication bot = await Context.Client.GetApplicationInfoAsync();

            EmbedBuilder e = new EmbedBuilder
            {
                Title = $"{bot.Name} Information",
                Description = $"\n**Author:** {bot.Owner.Username}\n" +
                $"**Created on:** {bot.CreatedAt.Date.ToShortDateString()} ({(DateTime.Now - bot.CreatedAt).Days} days old)\n" +
                $"**Description:** {bot.Description}\n",
                ThumbnailUrl = bot.IconUrl,
                Color = Colors.blue
            };

            await ReplyAsync(Context.User.Mention, embed: e);
        }

        // /serverinfo
        [Command("serverinfo")]
        [Summary("Displays basic information about the server")]
        public async Task ServerInfo()
        {
            IGuild g = Context.Guild;

            EmbedBuilder e = new EmbedBuilder
            {
                Title = $"{g.Name} Information",
                Description = $"**Owner:** {(await g.GetUserAsync(g.OwnerId)).Username}\n" +
                $"**Date Created:** {g.CreatedAt.Date.ToShortDateString()} ({(DateTime.Now - g.CreatedAt).Days} days old)\n" +
                $"**Region:** {g.VoiceRegionId}\n" +
                $"**Security Level:** {g.VerificationLevel}\n",
                ThumbnailUrl = g.IconUrl,
                Color = Colors.blue
            };

            await ReplyAsync(Context.User.Mention, embed: e);
        }

        // /formatting
        [Command("formatting")]
        [Alias("format")]
        [Summary("List chat format options")]
        public async Task Formatting()
        {
            EmbedBuilder e = new EmbedBuilder
            {
                Title = "__Basic Text Formatting__",
                Description = "```" +
                "**Bold Text**\n" +
                "*Italic Text*\n" +
                "__Underline Text__\n" +
                "~~Strike Out Text~~\n" +
                "`Code Snippet`\n" +
                "``\u200B`Code Block`\u200B``\n" +
                "```",
                Color = Colors.blue
            };

            await ReplyAsync(Context.User.Mention, embed: e);
        }

        // Gets the syntax of a command
        public string getSyntax(CommandInfo cmd)
        {
            string syntax = "";

            if (cmd.Parameters.Count > 0)
            {
                foreach (var p in cmd.Parameters)
                    if (p.IsOptional)
                        syntax += $"[{p.Name}] ";
                    else
                        syntax += $"<{p.Name}> ";
            }

            return syntax;
        }
    }

    // Private channel commands
    [Group("Channel"), RequireContext(ContextType.Guild)]
    [EnabledCommand]
    [Remarks("1")]
    public class Channel : InteractiveModuleBase
    {
        // /channel create
        [Command("create")]
        [Summary("Create a new private channel")]
        public async Task Create(
            [Summary("New channel name"), Remainder] string channelName)
        {
            IGuildUser u = Context.User as IGuildUser; // Get user executing command
            PerServerConfig config = PerServerConfig.Load(Context.Guild);
            bool isPublic = false;

            string[] input = channelName.Split(' ');
            if (input[0].ToUpper().Equals("PUBLIC"))
            {
                isPublic = true;
                channelName = string.Join(" ", input.Skip(1));
            }
            
            // TODO: Check if user has a channel
            if (!HasChannel(u, config)) // Check if user has a private channel already
            {
                int channelIndex = 2;
                if (config.PrivIndex >= 2 && config.PrivIndex <= (await Context.Guild.GetVoiceChannelsAsync()).Count && config.PrivIndex >= 2)
                    channelIndex = config.PrivIndex;

                // Create new voice channel and move it to private channel section
                IVoiceChannel v = await Context.Guild.CreateVoiceChannelAsync(channelName);
                await v.ModifyAsync(x => x.Position = channelIndex - 2);
                IRole prot = Context.Guild.Roles.Where(x => x.Name.ToUpper().Equals("PROTECTED")).FirstOrDefault();

                if (prot == null)
                    prot = await Context.Guild.CreateRoleAsync("Protected");
                
                // Allow-Connect permission
                OverwritePermissions p = OverwritePermissions.InheritAll; 
                p = p.Modify(connect: PermValue.Allow, manageChannel: PermValue.Allow, managePermissions: PermValue.Allow);

                // Deny-Connect permission
                OverwritePermissions everyone = OverwritePermissions.InheritAll;
                
                if (!isPublic)
                    everyone = everyone.Modify(connect: PermValue.Deny);
                else
                    everyone = everyone.Modify(connect: PermValue.Allow);

                // Add permissions to channel
                await v.AddPermissionOverwriteAsync(u, permissions: p);
                await v.AddPermissionOverwriteAsync(u.Guild.EveryoneRole, permissions: everyone);
                await v.AddPermissionOverwriteAsync(prot, p);
                
                // Add channel info to json and save 
                config.PrivChannels.Add(u.Id, v.Id);
                config.Save();
                
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        // /channel add
        [Command("add")]
        [Summary("Add a user to your private channel")]
        public async Task Add(
            [Summary("User to add to channel"), Remainder] params IGuildUser[] user)
        {
            IGuildUser u = Context.User as IGuildUser; // User who performed command
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (HasChannel(u, config)) // Check if user has a private channel
            {
                IVoiceChannel privChannel = await GetChannel(Context.Guild, u, config);
                
                // Connect permission
                OverwritePermissions p = OverwritePermissions.InheritAll;
                p = p.Modify(connect: PermValue.Allow);

                foreach (IGuildUser gu in user)
                {
                    try
                    {
                        await privChannel.AddPermissionOverwriteAsync(gu, permissions: p);
                    }
                    catch { }
                }
                    
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        // /channel remove
        [Command("remove")]
        [Summary("Remove a user from your private channel")]
        public async Task Remove(
            [Summary("User to remove from channel"), Remainder] params IGuildUser[] user)
        {
            IGuildUser u = Context.User as IGuildUser;
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (HasChannel(u, config)) // Check if user has a private channel
            {
                IVoiceChannel privChannel = await GetChannel(Context.Guild, u, config);

                foreach (IGuildUser gu in user)
                {
                    await privChannel.RemovePermissionOverwriteAsync(gu); // Remove user from permisions
                    
                    // Move user to AFK channel if they are removed from private channel
                    if (gu.VoiceChannel != null && gu.VoiceChannel.Id == privChannel.Id)
                    {
                        ulong id = (ulong)Context.Guild.AFKChannelId;
                        await gu.ModifyAsync(x => x.ChannelId = id);
                    }
                }

                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        // /channel destroy
        [Command("destroy")]
        [Summary("Delete your private channel")]
        public async Task Destroy()
        {
            IGuildUser u = Context.User as IGuildUser;
            PerServerConfig config = PerServerConfig.Load(Context.Guild);

            if (HasChannel(u, config)) // Check if user has a private channel
            {
                IVoiceChannel privChannel = await GetChannel(Context.Guild, u, config);
                await privChannel.DeleteAsync();

                config.PrivChannels.Remove(u.Id);
                config.Save();
                await CommandConfirmation.Success(Context);
            }
            else
            {
                await CommandConfirmation.Failure(Context);
            }
        }

        // Gets the private channel
        public async Task<IVoiceChannel> GetChannel(IGuild g, IGuildUser u, PerServerConfig config)
        {
            ulong channelID = config.PrivChannels[u.Id];
            IVoiceChannel privChannel = await g.GetVoiceChannelAsync(channelID);
            
            return privChannel;
        }

        // Check if user has a private channel
        public bool HasChannel(IGuildUser u, PerServerConfig config)
        {
            if (config.PrivChannels.ContainsKey(u.Id))
                return true;
            else
                return false;
        }
    }
}
