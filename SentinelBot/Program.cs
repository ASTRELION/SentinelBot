﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Reflection;
using System.Threading;
using System.IO;

using Discord;
using Discord.Commands;
using Discord.WebSocket;
using Microsoft.Extensions.DependencyInjection;

/* 
 * Sentinel Bot for Discord servers
*/

namespace SentinelBot
{
    class Program
    {
        static void Main(string[] args) =>
            new Program().Start().GetAwaiter().GetResult();
        
        private DiscordSocketClient client;
        private CommandService cmds;
        //private DependencyMap map = new DependencyMap();
        private IServiceProvider map;
        private List<PerServerConfig> configs = new List<PerServerConfig>();

        private string botStatus = "/help"; // Bot playing status
        private DateTime start = DateTime.Now;

        public async Task Start()
        {
            Configuration.EnsureExists();

            client = new DiscordSocketClient(new DiscordSocketConfig()
            {
                WebSocketProvider = Discord.Net.Providers.WS4Net.WS4NetProvider.Instance,
                LogLevel = LogSeverity.Info
            });

            cmds = new CommandService();
            await InstallCommands();
            client.Log += Log;

            //var services = ConfigureServices();

            // Login and connect
            await client.LoginAsync(TokenType.Bot, Configuration.Load().Token);
            await client.StartAsync();

            client.UserJoined += UserJoined;
            client.UserLeft += UserLeft;

            // Mostly used for afk checks
            client.UserVoiceStateUpdated += VoiceUpdated;
            client.UserIsTyping += UserTyping;
            client.UserUpdated += UserUpdated;

            client.UserBanned += Banned;

            client.ChannelDestroyed += ChannelDeleted;
            client.JoinedGuild += Connected;
            client.LeftGuild += Left;

            client.GuildUpdated += ServerUpdate;

            client.RoleUpdated += RoleUpdate;
            client.RoleDeleted += RoleDelete;

            // Ready event
            client.Ready += async delegate
            {
                IApplication bot = await client.GetApplicationInfoAsync();

                await client.CurrentUser.ModifyAsync(a =>
                {
                    Thread.Sleep(500);
                    client.SetGameAsync(botStatus);
                    a.Username = bot.Name;
                });

                foreach (SocketGuild g in client.Guilds)
                {
                    Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] {g.Name} | Connected. Owner: {g.Owner.Username}#{g.Owner.Discriminator}");

                    PerServerConfig.EnsureExists(g);
                    PerServerConfig config = PerServerConfig.Load(g);
                    List<string> available = Commands.GetCommands(cmds);

                    // Remove commands that no longer exist
                    foreach (string c in config.EnabledCommands)
                    {
                        if (!available.Contains(c.ToUpper()) && !c.Equals("ALL"))
                        {
                            config.EnabledCommands.Remove(c);
                        }
                    }
                }

                StartTimer();
            };
   
            await Task.Delay(-1);
        }

        

        private Task Log(LogMessage msg)
        {
            Console.WriteLine(msg.ToString());
            return Task.CompletedTask;
        }

        public async Task InstallCommands()
        {
            client.MessageReceived += HandleCommand;
            await cmds.AddModulesAsync(Assembly.GetEntryAssembly());
        }

        public async Task HandleCommand(SocketMessage messageParam)
        {
            // Only include non-system messages
            var message = messageParam as SocketUserMessage;
            
            if (message == null)
                return;
            
            int argPos = 0;
            var context = new CommandContext(client, message);

            if (!context.User.IsBot)
            {
                try
                {
                    PerServerConfig config = PerServerConfig.Load(context.Guild);
                    string upperMsg = message.Content.ToUpper();

                    if (config.AlertWords.Count > 0)
                    {
                        foreach (string word in config.AlertWords)
                            if (upperMsg.Contains(" " + word.ToUpper() + " ") ||
                                upperMsg.StartsWith(word.ToUpper() + " ") ||
                                upperMsg.EndsWith(" " + word.ToUpper()) ||
                                upperMsg.Equals(word.ToUpper()))
                            {
                                var dm = (await context.Guild.GetOwnerAsync()).GetOrCreateDMChannelAsync().Result;
                                await dm.SendMessageAsync($"__Keyword Alert:__\n{message.Author.ToString()} said \"{word}\"");
                                break;
                            }
                    }

                    if (config.BanWords.Count > 0)
                    {
                        foreach (string word in config.BanWords)
                            if (upperMsg.Contains(" " + word.ToUpper() + " ") ||
                                upperMsg.StartsWith(word.ToUpper() + " ") ||
                                upperMsg.EndsWith(" " + word.ToUpper()) ||
                                upperMsg.Equals(word.ToUpper()))
                            {
                                await message.DeleteAsync();
                                return;
                            }
                    }
                }
                catch { }
            }
            
            // Check if message is a command
            if (!(message.HasCharPrefix(Configuration.Load().Prefix, ref argPos) || message.HasMentionPrefix(client.CurrentUser, ref argPos)))
                return;

            // Execute and determine if command was successful
            var result = await cmds.ExecuteAsync(context, argPos, map);
            
            if (!result.IsSuccess)
            {
                await context.Channel.SendMessageAsync(result.ErrorReason);
            }
            else if (result.IsSuccess)
            {
                // Log command in console
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] " +
                    $"{context.Guild.Name} - {context.Channel.Name} | {message.Author.Username} : \"{message}\""); // Log command

                PerServerConfig config = PerServerConfig.Load(context.Guild);

                // Log command in log channel
                if (config.LogMode.Equals("ALL"))
                {
                    SocketGuild g = (SocketGuild)context.Guild;
                    await g.GetTextChannel(config.LogChannelID).SendMessageAsync($"{context.User} used \"{message}\"");
                }
            } 
        }

        public void StartTimer()
        {
            Thread t = new Thread(Execute);
            t.Start();
        }

        public void Execute()
        {
            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Starting timer...");

            while (true)
            {
                Thread.Sleep(10 * 60 * 1000); // Every 10 minutes
                Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] Deleting unprotected private channels...");

                // Cycle through all guilds
                foreach (IGuild g in client.Guilds)
                {
                    PerServerConfig config = PerServerConfig.Load(g);
                    List<List<ulong>> removables = new List<List<ulong>>();

                    foreach (var channels in config.PrivChannels)
                    {
                        IVoiceChannel privChannel = g.GetVoiceChannelAsync(channels.Value).Result;
                        IRole protectedRole = g.Roles.Where(x => x.Name.ToUpper().Equals("PROTECTED")).FirstOrDefault();
                        var p = privChannel.PermissionOverwrites;

                        if (privChannel.GetUsersAsync().Flatten().Result.Count() == 0)
                        {
                            try // Remove protected role from private channel if it has it
                            {
                                g.GetRole(p.Where(x => x.TargetId == protectedRole.Id).First().TargetId);
                                privChannel.RemovePermissionOverwriteAsync(protectedRole);
                            }
                            catch // Add channel to removal list if it doesn't have protected
                            {
                                privChannel.DeleteAsync();
                                config.PrivChannels.Remove(channels.Key);
                            }
                        }
                    }

                    config.Save();
                }
            }
        }

        #region Event Handlers

        public async Task ServerUpdate(SocketGuild g1, SocketGuild g2)
        {
            if (!g1.Name.Equals(g2.Name))
            {
                PerServerConfig config = PerServerConfig.Load(g1);

                if (!g2.Name.Equals(config.GuildName))
                {
                    config.GuildName = g2.Name;
                    config.Save();
                }
                
            }
        }

        public async Task RoleDelete(SocketRole r)
        {
            PerServerConfig config = PerServerConfig.Load(r.Guild);

            if (config.RolesOnJoin.Contains(r.Name))
                config.RolesOnJoin.Remove(r.Name);
        }

        private async Task RoleUpdate(SocketRole r1, SocketRole r2)
        {
            if (r1.Name != r2.Name)
            {
                PerServerConfig config = PerServerConfig.Load(r1.Guild);

                if (config.RolesOnJoin.Contains(r1.Name.ToUpper()))
                {
                    int index = config.RolesOnJoin.IndexOf(r1.Name.ToUpper());
                    config.RolesOnJoin[index] = r2.Name.ToUpper();
                    config.Save();
                }   
            }
        }

        // Create config when new server connected
        private async Task Connected(SocketGuild g)
        {
            PerServerConfig.EnsureExists(g);
        }

        // Remove config when server leaves
        private async Task Left(SocketGuild g)
        {
            PerServerConfig config = PerServerConfig.Load(g);
            config.Delete();
        }

        private async Task ChannelDeleted(SocketChannel s)
        {
            if (s is SocketTextChannel)
            {
                SocketTextChannel channel = s as SocketTextChannel;
                SocketGuild guild = channel.Guild;
                PerServerConfig config = PerServerConfig.Load(guild);

                // Check if logchannel was deleted, and replace it
                try
                {
                    SocketTextChannel test = guild.TextChannels.Where(x => x.Id == config.LogChannelID).First();
                }
                catch
                {
                    config.LogChannelID = guild.DefaultChannel.Id;
                    config.Save();
                }

                // Check if mainchat was deleted, and replace it
                try
                {
                    SocketTextChannel test = guild.TextChannels.Where(x => x.Id == config.MainChatID).First();
                }
                catch
                {
                    config.MainChatID = guild.DefaultChannel.Id;
                    config.Save();
                }
            } 
        }

        private async Task Banned(SocketUser u, SocketGuild g)
        {
            ModLog.LogMod(g.Owner, u as IGuildUser, "BAN", "", 0, PerServerConfig.Load(g));
        }

        private async Task UserJoined(SocketGuildUser u)
        {
            SocketGuild guild = u.Guild;
            PerServerConfig config = PerServerConfig.Load(guild);
            SocketTextChannel c = guild.GetTextChannel(config.MainChatID);     

            if (!config.Greeting.Equals(""))
            {
                string greeting = config.Greeting;
                greeting = greeting.Replace("[user]", $"{u.Username}");
                await c.SendMessageAsync(greeting);
            }

            if (config.RolesOnJoin.Count > 0)
            {
                foreach (string role in config.RolesOnJoin)
                {
                    IRole r = guild.Roles.Where(x => x.Name.ToUpper().Equals(role.ToUpper())).First();
                    await u.AddRoleAsync(r);
                }
            }
        }

        private async Task UserLeft(SocketGuildUser u)
        {
            SocketGuild guild = u.Guild;
            PerServerConfig config = PerServerConfig.Load(guild);
            SocketTextChannel c = guild.GetTextChannel(config.MainChatID);
            
            if (!config.Greeting.Equals(""))
            {
                string farewell = config.Farewell;
                farewell = farewell.Replace("[user]", $"{u.Username}");
                await c.SendMessageAsync(farewell);
            }
        }

        private async Task VoiceUpdated(SocketUser u, SocketVoiceState v1, SocketVoiceState v2)
        {
            SocketGuild guild = v1.VoiceChannel != null ? v1.VoiceChannel.Guild : v2.VoiceChannel.Guild;
            SocketVoiceChannel afkChannel = guild.AFKChannel;
            PerServerConfig config = PerServerConfig.Load(guild);
            ITextChannel main = guild.GetTextChannel(config.MainChatID);
            IRole afkRole;

            if (config.EnabledCommands.Contains("AFK"))
            {
                try
                {
                    afkRole = guild.Roles.Where(x => x.Name.ToUpper().Equals("AFK")).First() ?? null;
                }
                catch
                {
                    afkRole = null;
                }

                if (!guild.GetUser(u.Id).Roles.Contains(afkRole))
                {
                    // Set user as afk if they do not have AFK role
                    if (v2.VoiceChannel != null && v2.VoiceChannel == afkChannel)
                    {
                        await main.SendMessageAsync($"/afk {u}");
                    }
                }
                else
                {
                    // Unset user as afk if they have AFK role
                    if (v1.VoiceChannel == afkChannel && v2.VoiceChannel != afkChannel)
                    {
                        await main.SendMessageAsync($"/afk {u}");
                    }
                }
            }
        }

        private async Task UserUpdated(SocketUser u1, SocketUser u2)
        {
            // unAFK user if they come online
            if (u1.Status == UserStatus.AFK && u2.Status != UserStatus.AFK)
            {
                foreach (SocketGuild g in client.Guilds)
                {
                    SocketVoiceChannel afkChannel = g.AFKChannel;
                    PerServerConfig config = PerServerConfig.Load(g);
                    ITextChannel main = g.GetTextChannel(config.MainChatID);
                    IRole afkRole;

                    if (config.EnabledCommands.Contains("AFK"))
                    {
                        try
                        {
                            afkRole = g.Roles.Where(x => x.Name.ToUpper().Equals("AFK")).First() ?? null;
                        }
                        catch
                        {
                            afkRole = null;
                        }

                        if (g.GetUser(u1.Id).Roles.Contains(afkRole))
                        {
                            await main.SendMessageAsync($"/afk {u1}");
                        }
                    }
                }
            }
        }
        
        private async Task UserTyping(SocketUser u, ISocketMessageChannel c)
        {
            // unAFK user if they type
            foreach (SocketGuild g in client.Guilds)
            {
                SocketVoiceChannel afkChannel = g.AFKChannel;
                PerServerConfig config = PerServerConfig.Load(g);
                ITextChannel main = g.GetTextChannel(config.MainChatID);
                IRole afkRole;

                if (config.EnabledCommands.Contains("AFK"))
                {
                    try
                    {
                        afkRole = g.Roles.Where(x => x.Name.ToUpper().Equals("AFK")).First() ?? null;
                    }
                    catch
                    {
                        afkRole = null;
                    }

                    if (g.GetUser(u.Id).Roles.Contains(afkRole))
                    {
                        await main.SendMessageAsync($"/afk {u}");
                    }
                }   
            }
        }

        #endregion
    }
}