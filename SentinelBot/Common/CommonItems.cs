﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace SentinelBot
{
    public static class Colors
    {
        public static Color blue { get; } = new Color(0, 200, 255); // Default blue color
        public static Color green { get; } = new Color(0, 255, 0);
        public static Color yellow { get; } = new Color(255, 255, 0);
        public static Color red { get; } = new Color(255, 0, 0);
        public static Color purple { get; } = new Color(200, 0, 255);
        public static Color black { get; } = new Color(1, 1, 1);
        public static Color white { get; } = new Color(254, 254, 254);

        public static Color ServerColor(CommandContext context)
        {
            PerServerConfig config = PerServerConfig.Load(context.Guild);
            return new Color(config.ServerColor[0], config.ServerColor[1], config.ServerColor[2]);
        }
    }

    public static class CommandConfirmation
    {
        public static async Task Success(CommandContext context)
        {
            if (!context.User.IsBot)
                await context.Message.AddReactionAsync("\u2705");
        }

        public static async Task Failure(CommandContext context)
        {
            if (!context.User.IsBot)
                await context.Message.AddReactionAsync("\u274C");
        }

        public static async Task NotEnabled(CommandContext context)
        {
            if (!context.User.IsBot)
                await context.Message.AddReactionAsync("\u1F6AB");
        }
    }

    public static class Commands
    {
        public static List<string> GetCommands(CommandService cmdServ)
        {
            List<string> commands = new List<string>();

            foreach (var mod in cmdServ.Modules)
            {
                if (!mod.IsSubmodule && !mod.Name.ToUpper().Equals("SETTINGS") && !mod.Name.ToUpper().Equals("SERVERSETUP"))
                {
                    commands.Add(mod.Name.ToUpper());

                    if (!mod.Commands[0].Aliases[0].ToUpper().Contains(mod.Name.ToUpper()))
                        foreach (CommandInfo com in mod.Commands)
                            commands.Add(com.Name.ToUpper());
                }
            }

            return commands;
        }
    }

    public static class ModLog
    {
        // Log
        public static async Task LogMod(IGuildUser issuer, IGuildUser user, string punish, string reason, int duration, PerServerConfig config)
        {
            DateTime expiration = DateTime.UtcNow.AddMinutes(duration);

            EmbedBuilder e = new EmbedBuilder
            {
                Title = $"__{punish}__",
                Color = Colors.blue
            };

            if (duration == 0)
                e.Color = Colors.red;
            else
                e.Color = Colors.yellow;

            e.AddField(f =>
            {
                f.Name = "User:";
                f.Value = user.ToString();
                f.IsInline = true;
            });

            e.AddField(f =>
            {
                f.Name = "Moderator:";
                f.Value = issuer.ToString();
                f.IsInline = true;
            });

            e.AddField(f =>
            {
                f.Name = "Reason:";
                f.Value = reason.Equals("") ? "*No reason given*" : reason;
                f.IsInline = true;
            });

            e.AddField(f =>
            {
                f.Name = "Issued At:";
                f.Value = DateTime.UtcNow.ToShortDateString() + " " + DateTime.UtcNow.ToLongTimeString();
                f.IsInline = true;
            });

            e.AddField(f =>
            {
                f.Name = "Expires on:";
                f.Value = duration > 0 ? (expiration.ToShortDateString() +
                    " " + expiration.ToLongTimeString() + " UTC") : "*Permanent*";
                f.IsInline = true;
            });

            if (!user.IsBot)
            {
                // Send message to user
                var dm = user.CreateDMChannelAsync().Result;
                await dm.SendMessageAsync("You've received a punishment from a moderator:", embed: e);
            }

            // Send message to log channel if logging is enabled
            if (config.LogMode.Equals("MOD") || config.LogMode.Equals("ALL"))
            {
                ITextChannel channel = user.Guild.GetTextChannelAsync(config.LogChannelID).Result;
                await channel.SendMessageAsync("", embed: e);
            }

            // Start timer
            if (duration > 0)
            {
                Timers t = new Timers(user, punish.ToUpper(), duration);
                //config.Punishments.Add(user.Id, new List<string>{ "DEAFEN", expiration.ToOADate().ToString() });
            }
        }
    }
}
