﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace SentinelBot
{
    class CustomPerms : PreconditionAttribute
    {
        public override async Task<PreconditionResult> CheckPermissions(ICommandContext context, CommandInfo command, IDependencyMap map)
        {
            PerServerConfig config = PerServerConfig.Load(context.Guild);

            string perm = null;
            if (config.CommandPermissions.ContainsKey(command.Name.ToUpper()))
            {
                perm = config.CommandPermissions[command.Name.ToUpper()];

                GuildPermissions perms = GuildPermissions.All;
                GuildPermission p = perms.ToList().Where(x => x.ToString().ToUpper().Equals(perm.ToUpper())).FirstOrDefault();

                if ((await context.Guild.GetUserAsync(context.User.Id)).GuildPermissions.Has(p))
                {
                    return PreconditionResult.FromSuccess();
                }
                else
                {
                    return PreconditionResult.FromError("You do not have permission to do that.");
                }
            }
            else
            {
                return PreconditionResult.FromSuccess();
            }
        }
    }
    
    class EnabledCommand : PreconditionAttribute
    {
        public override async Task<PreconditionResult> CheckPermissions(ICommandContext context, CommandInfo command, IDependencyMap map)
        {
            PerServerConfig config = PerServerConfig.Load(context.Guild);
            
            string c = config.EnabledCommands
                .Where(x => x.ToUpper().Equals(command.Aliases[0].ToUpper().Substring(0, command.Aliases[0].IndexOf(' ') != -1 ? command.Aliases[0].IndexOf(' ') : command.Aliases[0].Length))).FirstOrDefault();

            if (c == null)
                return PreconditionResult.FromError("");

            return PreconditionResult.FromSuccess();
        }
    }
}
