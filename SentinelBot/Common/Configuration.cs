﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;
using System.IO;

namespace SentinelBot
{
    class Configuration
    {
        public static string FileName { get; private set; } = "Data/_config.json";
        public string Token { get; set; } = "";
        public char Prefix { get; set; } = '/';

        public static void EnsureExists()
        {
            string file = Path.Combine(AppContext.BaseDirectory, FileName);

            if (!File.Exists(file))
            {
                var config = new Configuration();

                Console.WriteLine("Enter your token: ");
                string token = Console.ReadLine();

                Console.WriteLine("Enter your command prefix: ");
                string pref = Console.ReadLine();

                config.Token = token;
                config.Save();
            }

            Console.WriteLine("Configuration loaded.");
        }

        public void Save()
        {
            string file = Path.Combine(AppContext.BaseDirectory, FileName);
            File.WriteAllText(file, ToJson());
        }

        public static Configuration Load()
        {
            string file = Path.Combine(AppContext.BaseDirectory, FileName);
            return JsonConvert.DeserializeObject<Configuration>(File.ReadAllText(file));
        }
        
        public string ToJson() => JsonConvert.SerializeObject(this, Formatting.Indented);
    }
}
