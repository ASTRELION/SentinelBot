﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Newtonsoft.Json;

using Discord;
using Discord.Commands;

namespace SentinelBot
{
    public class PerServerConfig
    {
        private static string FileName { get; set; } = "Data\\"; // Path

        // GUILD INFO
        public string GuildName { get; set; }
        public ulong GuildID { get; set; } // Guild ID
        public ulong MainChatID { get; set; }
        public ulong LogChannelID { get; set; }

        public string LogMode { get; set; } = "OFF"; // OFF MOD ALL

        public string Greeting { get; set; } = "";
        public string Farewell { get; set; } = "";

        public int[] ServerColor { get; set; } = { 0, 200, 255 };

        public List<string> RolesOnJoin { get; set; } = new List<string>();
        
        public List<string> EnabledCommands { get; set; } = new List<string> { "HELP" }; // Enabled commands
        public Dictionary<string, string> CommandPermissions { get; set; } = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase) // [Index][CommandName, Permission]
        {
            { "RESET","ChangeNickname"},
            { "RESETOTHER", "ManageNicknames" },
            { "MOVETO", "Connect" },
            { "DEAFEN", "DeafenMembers" },
            { "MUTE", "MuteMembers" },
            { "TEXTMUTE", "MuteMembers" },
            { "BAN", "BanMembers" },
            { "UNBAN", "BanMembers" },
            { "SAY", "ManageMessages" },
            { "PURGE", "ManageMessages" },
            { "MOVEALL", "MoveMembers" }
        }; // Default Permissions

        // PRIVATE CHANNELS
        public int PrivIndex { get; set; } = 2; // Private Channel Number (Index of channel)
        public Dictionary<ulong, ulong> PrivChannels { get; set; } = new Dictionary<ulong, ulong>(); // [Index][UserID,ChannelID]

        // VOTING
        public Dictionary<string, int> Votes { get; set; } = new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase); // [Index][Item,Votes]
        public List<ulong> Voters { get; set; } = new List<ulong>(); // List of users who have voted

        // KEYWORDS
        public List<string> AlertWords { get; set; } = new List<string>();
        public List<string> BanWords { get; set; } = new List<string>();

        // MOD INFO
        public Dictionary<ulong, List<string[]>> Punishments { get; set; } = new Dictionary<ulong, List<string[]>>(); // [UserID][Punishment,ExpirationTime]

        // Create file
        public static void EnsureExists(IGuild g)
        {
            string file = AppContext.BaseDirectory + FileName + g.Id.ToString() + ".json";

            if (!File.Exists(file))
            {
                PerServerConfig config = new PerServerConfig();
                config.GuildName = g.Name;
                config.GuildID = g.Id;
                config.LogChannelID = g.DefaultChannelId;
                config.MainChatID = g.DefaultChannelId;
                config.Save();
            }
            else
            {
                PerServerConfig config = Load(g);
                config.Save();
            }

            Console.WriteLine($"[{DateTime.Now.ToString("HH:mm:ss")}] {g.Name} | Config Loaded.");
        }

        // Save guild settings to file
        public void Save()
        {
            string file = AppContext.BaseDirectory + FileName + this.GuildID.ToString() + ".json";
            File.WriteAllText(file, ToJson());
        }

        public void Delete()
        {
            string file = AppContext.BaseDirectory + FileName + this.GuildID.ToString() + ".json";
            File.Delete(file);
        }

        // Load guild settings into object
        public static PerServerConfig Load(IGuild g)
        {
            string file = AppContext.BaseDirectory + FileName + g.Id.ToString() + ".json";
            return JsonConvert.DeserializeObject<PerServerConfig>(File.ReadAllText(file));
        }

        public string ToJson() => JsonConvert.SerializeObject(this, Formatting.Indented);
    }
}
