﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Threading;

using Discord;
using Discord.Commands;
using Discord.WebSocket;

namespace SentinelBot
{
    class Timers
    {
        private IGuildUser user;
        private string punish;
        private int min;

        public Timers(IGuildUser user, string punish, int min)
        {
            this.user = user;
            this.punish = punish;
            this.min = min;

            Thread t = new Thread(Time);
            t.Start();
        }

        public void Time()
        {
            Thread.Sleep(min * 60 * 1000);

            // Remove punishment after time
            switch (punish)
            {
                case "MUTE":
                    user.ModifyAsync(x => x.Mute = false);
                    PerServerConfig config = PerServerConfig.Load(user.Guild);
                    
                    break;

                case "TEXTMUTE":
                    foreach (ITextChannel channels in user.Guild.GetTextChannelsAsync().Result)
                    {
                        channels.RemovePermissionOverwriteAsync(user);
                    }
                    break;

                case "DEAFEN":
                    user.ModifyAsync(x => x.Deaf = false);
                    break;

                case "BAN":
                    user.Guild.RemoveBanAsync(user);
                    break;

                default:
                    break;
            }
        }
    }
}
